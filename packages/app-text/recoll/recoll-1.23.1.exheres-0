# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache

SUMMARY="A personal full text search tool for Unix/Linux"
HOMEPAGE="https://www.lesbonscomptes.com/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

BUGS_TO="alip@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    camelcase [[ description = [ Enable CamelCase word splitting ] ]]
    qt5
    spell
    x11mon [[ requires = inotify ]]
"

DEPENDENCIES="
    build+run:
        dev-db/xapian-core
        qt5? (
            x11-libs/qtbase:5[gui]
            x11-libs/qtwebkit:5
        )
        spell? ( app-spell/aspell )
        x11mon? (
            x11-libs/libX11
            x11-libs/libICE
            x11-libs/libSM
        )
    suggestion:
        app-doc/catdoc      [[ description = [ index ms powerpoint and excel documents  ] ]]
        app-text/djvu       [[ description = [ index djvu documents ] ]]
        app-text/poppler    [[ description = [ index pdf documents ] ]]
        app-text/pstotext   [[ description = [ index postscript documents ] ]]
        app-text/tesseract  [[ description = [ automatically perform OCR on image-only pdfs ] ]]
        app-text/texlive    [[ description = [ index tex documents ] ]]
        app-text/unrtf      [[ description = [ index rtf documents ] ]]
        base/antiword       [[ description = [ index ms word documents ] ]]
        dev-libs/libxslt    [[ description = [ index xml files ] ]]
        dev-python/mutagen  [[ description = [ index tags of audio files ] ]]
        dev-python/pychm    [[ description = [ index chm documents ] ]]
        media-libs/ExifTool [[ description = [ index document formats supported by exiftool ] ]]
        office-libs/libwpd  [[ description = [ index wordperfect documents ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-recollq
    --disable-python-module
    --disable-userdoc
    --disable-xadump
    --with-inotify
    --without-fam
    --without-qzeitgeist
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'qt5 qtgui'
    'qt5 webkit'
    x11mon
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'spell aspell'
)

src_configure() {
    export QMAKE=qmake-qt5

    default
}

src_install() {
    # strip is bad mmkay?
    STRIP="$(type -P true)" default

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    option qt5 && freedesktop-desktop_pkg_postinst
    option qt5 && gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    option qt5 && freedesktop-desktop_pkg_postrm
    option qt5 && gtk-icon-cache_pkg_postrm
}

