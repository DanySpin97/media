# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gconf

SUMMARY="Set of well maintained plugins for gstreamer"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    doc
    examples
    gstreamer_plugins:
        aalib      [[ description = [ Output videos as black/white ASCII art ] ]]
        caca       [[ description = [ Colored ASCII art video output using libcaca ] ]]
        cairo      [[ description = [ Cairo-based text overlaying (not suited for subtitles) and video-to-stream rendering ] ]]
        dv         [[ description = [ DV (Digital Video) demuxing and decoding using libdv ] ]]
        flac       [[ description = [ Free Lossless Audio Codec support ] ]]
        gdk-pixbuf [[ description = [ GdkPixbuf-based image decoding, scaling and image output as GdkPixbuf ] ]]
        gtk3       [[ description = [ GTK+3 video sink ] ]]
        jack       [[ description = [ Support for audio input/output via the Jack Audio Connection Kit ] ]]
        lame       [[ description = [ MP3 audio encoding using LAME ] ]]
        mp2        [[ description = [ MP2 audio encoding using TwoLAME ] ]]
        mpg123     [[ description = [ MP3 audio decoding using libmpg123 ] ]]
        oss        [[ description = [ Adds support for OSSv4 ] ]]
        pulseaudio [[ description = [ Audio input, output and mixing using PulseAudio ] ]]
        qt5        [[ description = [ Qt5 Qml GL video sink ] ]]
        shout      [[ description = [ Send data to a SHOUTcast-compatible server (e.g. Icecast) ] ]]
        soup       [[ description = [ HTTP source handling using libsoup ] ]]
        speex      [[ description = [ Speex audio encoding and decoding using the speex library ] ]]
        taglib     [[ description = [ APEv2 and ID3v2 writing using TagLib ] ]]
        v4l        [[ description = [ Capture, radio, and remote controller device support ] ]]
        vpx        [[ description = [ VP8 encoder/decoder support ] ]]
        wavpack    [[ description = [ Support for the lossy/lossless audio format WavPack ] ]]
        X          [[ description = [ X display capturing ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        app-arch/bzip2
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.17]
        gnome-desktop/libgudev[>=147]
        media-libs/gstreamer:1.0[>=${PV}]
        media-libs/libpng:=[>=1.5.1]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        sys-libs/zlib
        examples? ( x11-libs/gtk+:3 )
        gstreamer_plugins:aalib? ( media-libs/aalib )
        gstreamer_plugins:caca? ( media-libs/libcaca )
        gstreamer_plugins:cairo? ( x11-libs/cairo[>=1.10.0] )
        gstreamer_plugins:dv? ( media-libs/libdv[>=0.100] )
        gstreamer_plugins:flac? ( media-libs/flac[>=1.1.4] )
        gstreamer_plugins:gdk-pixbuf? ( x11-libs/gdk-pixbuf:2.0[>=2.8.0] )
        gstreamer_plugins:gtk3? (
            media-plugins/gst-plugins-base[gstreamer_plugins:opengl]
            x11-libs/gtk+:3[X]
        )
        gstreamer_plugins:jack? ( media-sound/jack-audio-connection-kit[>=0.120.1] )
        gstreamer_plugins:lame? ( media-sound/lame )
        gstreamer_plugins:mp2? ( media-libs/twolame[>=0.3.10] )
        gstreamer_plugins:mpg123? ( media-sound/mpg123[>=1.13] )
        gstreamer_plugins:pulseaudio? ( media-sound/pulseaudio[>=2.0] )
        gstreamer_plugins:qt5? (
            x11-libs/qtbase:5[>=5.4]
            x11-libs/qtdeclarative:5[>=5.4]
            x11-libs/qtx11extras:5[>=5.4]
        )
        gstreamer_plugins:shout? ( media-libs/libshout[>=2.0] )
        gstreamer_plugins:soup? ( gnome-desktop/libsoup:2.4[>=2.48.0] )
        gstreamer_plugins:speex? ( media-libs/speex[>=1.1.6] )
        gstreamer_plugins:taglib? ( media-libs/taglib[>=1.5] )
        gstreamer_plugins:wavpack? ( media-sound/wavpack[>=4.60.0] )
        gstreamer_plugins:v4l? ( media-libs/v4l-utils )
        gstreamer_plugins:vpx? ( media-libs/libvpx:=[>=1.4.0] )
        gstreamer_plugins:X? (
            x11-libs/libX11
            x11-libs/libXdamage
            x11-libs/libXext
            x11-libs/libXfixes
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        !media-plugins/gst-plugins-bad:1.0[<1.2.0] [[
            note = [ Maybe some 1.1 versions work too ]
            description = [ Provides DTMF too ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-experimental'

    # core plugins
    '--enable-alpha'
    '--enable-apetag'
    '--enable-audiofx'
    '--enable-audioparsers'
    '--enable-auparse'
    '--enable-autodetect'
    '--enable-avi'
    '--enable-cutter'
    '--enable-debugutils'
    '--enable-deinterlace'
    '--enable-dtmf'
    '--enable-effectv'
    '--enable-equalizer'
    '--enable-flv'
    '--enable-flx'
    '--enable-goom'
    '--enable-goom2k1'
    '--enable-icydemux'
    '--enable-id3demux'
    '--enable-imagefreeze'
    '--enable-isomp4'
    '--enable-interleave'
    '--enable-isomp4'
    '--enable-law'
    '--enable-level'
    '--enable-matroska'
    '--enable-monoscope'
    '--enable-multifile'
    '--enable-multipart'
    '--enable-replaygain'
    '--enable-rtp'
    '--enable-rtpmanager'
    '--enable-rtsp'
    '--enable-shapewipe'
    '--enable-smpte'
    '--enable-spectrum'
    '--enable-udp'
    '--enable-videobox'
    '--enable-videocrop'
    '--enable-videofilter'
    '--enable-videomixer'
    '--enable-wavenc'
    '--enable-wavparse'
    '--enable-y4m'
    '--with-gudev'

    # (de)compression support
    '--enable-bz2'
    '--enable-zlib'

    '--enable-jpeg'
    '--enable-libpng'
    '--enable-orc'

    # Windows/Solaris/OS X specific
    '--disable-directsound'
    '--disable-osx-audio'
    '--disable-osx-video'
    '--disable-waveform'

    # unpackaged dependencies
    '--disable-dv1394'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'debug'
    'doc gtk-doc'
    'examples'

    # optional plugins
    'gstreamer_plugins:aalib'
    'gstreamer_plugins:caca libcaca'
    'gstreamer_plugins:cairo'
    'gstreamer_plugins:dv libdv'
    'gstreamer_plugins:flac'
    'gstreamer_plugins:gdk-pixbuf'
    'gstreamer_plugins:gtk3'
    'gstreamer_plugins:jack'
    'gstreamer_plugins:lame'
    'gstreamer_plugins:mp2 twolame'
    'gstreamer_plugins:mpg123'
    'gstreamer_plugins:oss'
    'gstreamer_plugins:oss oss4'
    'gstreamer_plugins:pulseaudio pulse'
    'gstreamer_plugins:qt5 qt'
    'gstreamer_plugins:shout shout2'
    'gstreamer_plugins:soup'
    'gstreamer_plugins:speex'
    'gstreamer_plugins:taglib'
    'gstreamer_plugins:v4l gst-v4l2'
    'gstreamer_plugins:vpx'
    'gstreamer_plugins:wavpack'
    'gstreamer_plugins:X x'
)

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS
    unset DISPLAY

    # needed for the souphttpsrc test
    export GSETTINGS_BACKEND=memory
    edo sed -e 's/SOUP_ADDRESS_ANY_PORT/60000/g' -i tests/check/elements/souphttpsrc.c
    esandbox allow_net "inet:0.0.0.0@60000"
    esandbox allow_net --connect "inet:127.0.0.1@60000"

    # udpsink/rtpaux tests use multiple ports
    edo rm tests/check/elements/{udpsink,rtpaux}.c

    default

    esandbox disallow_net "inet:0.0.0.0@60000"
    esandbox disallow_net --connect "inet:127.0.0.1@60000"
}

