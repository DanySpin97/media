# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require udev-rules

SUMMARY="A shared library to access the contents of an iPod"
HOMEPAGE="http://gtkpod.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/gtkpod/${PNV}.tar.bz2"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc mono"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.21]
        sys-devel/gettext
        doc? ( dev-doc/gtk-doc )
    build+run:
        app-pda/libimobiledevice[>=1.1.5]
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.16.0]
        dev-libs/libplist[>=1.0]
        dev-libs/libxml2:2.0
        sys-apps/sg3_utils
        virtual/usb:1
        x11-libs/gdk-pixbuf:2.0[>=2.6.0] [[ note = [ Only needed for ArtworkDB ] ]]
        mono? (
            dev-lang/mono[>=1.9.1]
            gnome-bindings/gtk-sharp:3
        )
"

# One test uses taglib

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libxml
    --enable-udev
    --with-udev-dir=${UDEVDIR}
    # ArtworkDB
    --enable-gdk-pixbuf
    --with-libimobiledevice
    --without-hal
    --without-python
    --disable-pygobject
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc gtk-doc' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( mono )

src_prepare() {
    default

    if optionq mono ; then
        # The only client of libgpod[mono] is media-sound/banshee which is already
        # ported to Gtk#3. Use gtk-sharp:3 here to avoid assembly conflict between
        # gtk-sharp-2.dll and gtk-sharp-3.dll when building Banshee
        edo sed \
            -e 's/\(\(glib\|gtk\)-sharp\)-2.0/\1-3.0/g' \
            -i configure \
            -i bindings/mono/libgpod-sharp/libgpod-sharp.pc.in
        # resolve references to System.DateTime and GLib.GException
        edo sed \
            -e '/^\tusing GLib;$/d' \
            -e 's/\(GException\)/GLib.\1/g' \
            -i bindings/mono/libgpod-sharp/Artwork.cs
    fi
}

src_install() {
    default
    edo rmdir "${IMAGE}"/tmp
}

