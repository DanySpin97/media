# Copyright 2011 Alex Elsayed <eternaleye@gmail.com>
# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require alternatives

if ever is_scm; then
    SCM_REPOSITORY="https://git.videolan.org/git/${PN}.git"
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ] scm-git
else
    DOWNLOADS="https://ftp.videolan.org/pub/videolan/${PN}/${PV}/${PNV}.tar.bz2"
fi

export_exlib_phases src_install

SUMMARY="Advanced Access Content System demo implementation"
DESCRIPTION="
This research project provides, through an open-source library, a way to
understand how the AACS works.
This research project is mainly developed by an international team of
developers from Doom9.
NB: this project doesn't offer any key or certificate that could be used to
decode encrypted copyrighted material.
"
HOMEPAGE="https://www.videolan.org/developers/${PN}.html"

BUGS_TO="pyromanaic@thwitt.de"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
    build+run:
        dev-libs/libgcrypt
        dev-libs/libgpg-error
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-extra-warnings
    --disable-optimizations
    --disable-static
    --disable-werror
)

libaacs_src_install() {
    local lib_path

    default

    lib_path=$(echo "${IMAGE}"/usr/$(exhost --target)/lib/libaacs.so.0.*)
    lib_path=${lib_path#${IMAGE}}

    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/${PN}.so{,.0}
    alternatives_for \
        libaacs libaacs 0 \
        /usr/$(exhost --target)/lib/libaacs.so   ${lib_path} \
        /usr/$(exhost --target)/lib/libaacs.so.0 ${lib_path}
}

